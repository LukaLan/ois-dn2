var Klepet = function(socket) {
  this.socket = socket;
};

Klepet.prototype.posljiSporocilo = function(kanal, besedilo) {
  var sporocilo = {
    kanal: kanal,
    besedilo: besedilo
  };
  this.socket.emit('sporocilo', sporocilo);
};

// prototip funkcije za posiljanje zasebnih sporocil
Klepet.prototype.zasebnoSporocilo = function(prejemnik, besedilo) {
  var sporocilo = {
    prejemnik: prejemnik,
    besedilo: besedilo
  };
  this.socket.emit('zasebnoSporocilo', sporocilo);
};

Klepet.prototype.spremeniKanal = function(kanal) {
  this.socket.emit('pridruzitevZahteva', {
    novKanal: kanal
  });
};

// prototip funkcije za pridruzitev kanalu z geslom
Klepet.prototype.spremeniKanalGeslo = function(kanal, geslo) {
  this.socket.emit('pridruzitevGesloZahteva', {
    novKanal: kanal,
    geslo: geslo
  });
};

Klepet.prototype.procesirajUkaz = function(ukaz) {
  var besede = ukaz.split(' ');
  ukaz = besede[0].substring(1, besede[0].length).toLowerCase();
  var sporocilo = false;

  switch(ukaz) {
    case 'pridruzitev':
      besede.shift();
      var kanal = besede.join(' ');
      
      // preverjanje pravilnosti zapisa ukaza
      if(besede.length == 2 &&
         besede[0].charAt(0) == '"' &&
         besede[0].charAt(besede[0].length - 1) == '"' &&
         besede[1].charAt(0) == '"' &&
         besede[1].charAt(besede[1].length - 1) == '"'
         ) {
        kanal = besede[0].substring(1, besede[0].length - 1);
        var geslo = besede[1].substring(1, besede[1].length - 1);
        // klic funkcije za kanal z geslom
        this.spremeniKanalGeslo(kanal, geslo);
      }
      else if(besede.length == 1) {
        // klic funkcije za prosto dostopen kanal
        this.spremeniKanal(kanal);
      }
      else {
        sporocilo = 'Neznan ukaz.';
      }
      break;
    case 'vzdevek':
      besede.shift();
      var vzdevek = besede.join(' ');
      this.socket.emit('vzdevekSpremembaZahteva', vzdevek);
      break;
    // case, ko je ukaz zasebno
    case 'zasebno':
      // preveri, ali je dovolj argumentov
      if(besede.length < 3) {
        sporocilo = 'Neznan ukaz.';
        break;
      }
      
      // ce je sporocilo daljse od ene besede, te besede zdruzi v en string
      var vecBesed = besede[2];
      if(besede.length > 3) {
        for(var i = 3; i < besede.length; i++) {
          vecBesed += ' ';
          vecBesed += besede[i];
        }
      }
      
      // preveri, ali je prejemnik in sporocilo napisano v navednicah
      if(besede[1].charAt(0) == '"' &&
         besede[1].charAt(besede[1].length - 1) == '"' &&
         vecBesed.charAt(0) == '"' &&
         vecBesed.charAt(vecBesed.length - 1) == '"'
         ) {
        var prejemnik = besede[1].substring(1, besede[1].length - 1);
        var besedilo = vecBesed.substring(1, vecBesed.length - 1);
        // izvede funkcijo
        this.zasebnoSporocilo(prejemnik, besedilo);
      }
      else {
        sporocilo = 'Neznan ukaz.';
      }
      break;
    default:
      sporocilo = 'Neznan ukaz.';
      break;
  };

  return sporocilo;
};