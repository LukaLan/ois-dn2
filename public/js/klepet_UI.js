// globalni seznam vulgarnih besed
var seznamVulgarnihBesed = vrniSeznamVulgarnihBesed();

function divElementEnostavniTekst(sporocilo) {
  return $('<div style="font-weight: bold"></div>').text(sporocilo);
}

function divElementHtmlTekst(sporocilo) {
  return $('<div></div>').html('<i>' + sporocilo + '</i>');
}

//funkcija, ki zaradi moznosti XSS napada zamenja < (manjse kot) z njrgovo HTML entiteto &lt; (ali &#60;) in > (vecje kot) z &gt; (ali &#62;)
function ltgt(tekst) {
  
  var manjse = /</g, vecje = />/g;
 
  tekst = tekst.replace(manjse, "&lt;").replace(vecje, "&gt;");
  
  return tekst;
}

//funkcija, ki zamenja zaporedja znakov s smeški
function smeski(tekst) {
  tekst = tekst.replace(/;\)/g, '<img src="https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/wink.png" />')
               .replace(/:\)/g, '<img src="https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/smiley.png" />')
               .replace(/\(y\)/g, '<img src="https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/like.png" />')
               .replace(/:\*/g, '<img src="https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/kiss.png" />')
               .replace(/:\(/g, '<img src="https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/sad.png" />');
  
  return tekst;
}

function procesirajVnosUporabnika(klepetApp, socket) {
  var sporocilo = $('#poslji-sporocilo').val();
  // poslje sporocilo skozi ltgt funkcijo
  sporocilo = ltgt(sporocilo);
  var sistemskoSporocilo;

  if (sporocilo.charAt(0) == '/') {
    sistemskoSporocilo = klepetApp.procesirajUkaz(sporocilo);
    if (sistemskoSporocilo) {
      $('#sporocila').append(divElementHtmlTekst(sistemskoSporocilo));
    }
  } else {
    // filtriranje sporocila pred posiljanjem
    sporocilo = filtrirajVulgarneBesede(sporocilo);
    sporocilo = smeski(sporocilo);
    klepetApp.posljiSporocilo(kanal, sporocilo);
    // sporocilo procesira kot HTML
    $('#sporocila').append(divElementHtmlTekst(sporocilo));
    $('#sporocila').scrollTop($('#sporocila').prop('scrollHeight'));
  }

  $('#poslji-sporocilo').val('');
}

// spremenljivki vzdevek in kanal
var vzdevek;
var kanal;
// funkcija za pridobivanje seznama vulgarnih besed iz datoteke swearWords.csv
function vrniSeznamVulgarnihBesed() {
  
  var seznam = [];
  
  $.ajax({
    url: "../swearWords.csv",
    type: 'get',
    dataType: 'text',
    async: false,
    success: function(vseBesede) {
      seznam = vseBesede.split(",");
    }
  });
  
  return seznam;
}

// funkcija za filtriranje vulgarnih besed
function filtrirajVulgarneBesede(tekst) {
  
  for(var i = 0; i < seznamVulgarnihBesed.length; i++) {
    var vulgarnaBeseda = new RegExp("\\b" + seznamVulgarnihBesed[i] + "\\b", "gi");
    var vulgarnaBesedaZvezdice = "";
    
    for(var j = 0; j < seznamVulgarnihBesed[i].length; j++) {
      vulgarnaBesedaZvezdice += "*";
    }
    
    tekst = tekst.replace(vulgarnaBeseda, vulgarnaBesedaZvezdice);
  }
  
  return tekst;
}

var socket = io.connect()

$(document).ready(function() {
  var klepetApp = new Klepet(socket);

  socket.on('vzdevekSpremembaOdgovor', function(rezultat) {
    var sporocilo;
    if (rezultat.uspesno) {
      // spremenljivka vzdevek se posodobi na trenutni vzdevek
      vzdevek = rezultat.vzdevek;
      // nastavljanje besedila v div-u kanal na vzdevek + @ + kanal, ko se spremeni vzdevek
      $('#kanal').text(vzdevek + " @ " + kanal);
      sporocilo = 'Prijavljen si kot ' + rezultat.vzdevek + '.';
    } else {
      sporocilo = rezultat.sporocilo;
    }
    $('#sporocila').append(divElementHtmlTekst(sporocilo));
  });

  socket.on('pridruzitevOdgovor', function(rezultat) {
    // spremenljivka kanal se posodobi na trenutni kanal
    kanal = rezultat.kanal;
    // nastavljanje besedila v div-u kanal na vzdevek + @ + kanal, ko se spremeni kanal
    $('#kanal').text(vzdevek + " @ " + kanal);
    $('#sporocila').append(divElementHtmlTekst('Sprememba kanala.'));
  });

  socket.on('sporocilo', function (sporocilo) {
    // doda sporocilo kot HTML
    var novElement = $('<div style="font-weight: bold"></div>').html(sporocilo.besedilo);
    $('#sporocila').append(novElement);
  });

  socket.on('kanali', function(kanali) {
    $('#seznam-kanalov').empty();

    for(var kanal in kanali) {
      kanal = kanal.substring(1, kanal.length);
      if (kanal != '') {
        $('#seznam-kanalov').append(divElementEnostavniTekst(kanal));
      }
    }

    $('#seznam-kanalov div').click(function() {
      klepetApp.procesirajUkaz('/pridruzitev ' + $(this).text());
      $('#poslji-sporocilo').focus();
    });
  });

  setInterval(function() {
    socket.emit('kanali');
  }, 1000);
  
  // funkcija, ki prejme seznam vseh uporabnikov in jih doda pod seznam-uporabnikov
  socket.on('uporabniki', function(uporabniki) {
    $('#seznam-uporabnikov').empty();
    
    var seznamUporabnikov = uporabniki.split(", ");
    for(var uporabnik in uporabniki) {
        $('#seznam-uporabnikov').append(divElementEnostavniTekst(seznamUporabnikov[uporabnik]));
    }
  });

  setInterval(function() {
    socket.emit('uporabniki');
  }, 1000);

  $('#poslji-sporocilo').focus();

  $('#poslji-obrazec').submit(function() {
    procesirajVnosUporabnika(klepetApp, socket);
    return false;
  });
});