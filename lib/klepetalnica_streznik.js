var socketio = require('socket.io');
var io;
var stevilkaGosta = 1;
var vzdevkiGledeNaSocket = {};
var uporabljeniVzdevki = [];
var trenutniKanal = {};
// array gesel vseh kanalov
var seznamGesel = [];

exports.listen = function(streznik) {
  io = socketio.listen(streznik);
  io.set('log level', 1);
  io.sockets.on('connection', function (socket) {
    stevilkaGosta = dodeliVzdevekGostu(socket, stevilkaGosta, vzdevkiGledeNaSocket, uporabljeniVzdevki);
    pridruzitevKanalu(socket, 'Skedenj');
    vrniUporabnike(socket);
    obdelajPosredovanjeSporocila(socket, vzdevkiGledeNaSocket);
    obdelajPosredovanjeZasebnegaSporocila(socket, uporabljeniVzdevki, vzdevkiGledeNaSocket);
    obdelajZahtevoZaSprememboVzdevka(socket, vzdevkiGledeNaSocket, uporabljeniVzdevki);
    obdelajPridruzitevKanalu(socket);
    socket.on('kanali', function() {
      socket.emit('kanali', io.sockets.manager.rooms);
    });
    // funkcija, ki pošilja seznam uporabnikov na trenutnem kanalu
    socket.on('uporabniki', function() {
      socket.emit('uporabniki', vrniUporabnike(socket));
    });
    
    obdelajOdjavoUporabnika(socket, vzdevkiGledeNaSocket, uporabljeniVzdevki);
  });
};

function dodeliVzdevekGostu(socket, stGosta, vzdevki, uporabljeniVzdevki) {
  var vzdevek = 'Gost' + stGosta;
  vzdevki[socket.id] = vzdevek;
  socket.emit('vzdevekSpremembaOdgovor', {
    uspesno: true,
    vzdevek: vzdevek
  });
  uporabljeniVzdevki.push(vzdevek);
  return stGosta + 1;
}

function pridruzitevKanalu(socket, kanal) {
  socket.join(kanal);
  trenutniKanal[socket.id] = kanal;
  socket.emit('pridruzitevOdgovor', {kanal: kanal});
  socket.broadcast.to(kanal).emit('sporocilo', {
    besedilo: vzdevkiGledeNaSocket[socket.id] + ' se je pridružil kanalu ' + kanal + '.'
  });
  var uporabnikiNaKanalu = io.sockets.clients(kanal);
  if (uporabnikiNaKanalu.length > 1) {
    var uporabnikiNaKanaluPovzetek = 'Trenutni uporabniki na kanalu ' + kanal + ': ';
    for (var i in uporabnikiNaKanalu) {
      var uporabnikSocketId = uporabnikiNaKanalu[i].id;
      if (uporabnikSocketId != socket.id) {
        if (i > 0) {
          uporabnikiNaKanaluPovzetek += ', ';
        }
        uporabnikiNaKanaluPovzetek += vzdevkiGledeNaSocket[uporabnikSocketId];
      }
    }
    uporabnikiNaKanaluPovzetek += '.';
    socket.emit('sporocilo', {besedilo: uporabnikiNaKanaluPovzetek});
  }
}

//funkcija, ki vraca seznam uporabnikov
function vrniUporabnike(socket) {
  
  var kanal = trenutniKanal[socket.id];
  var uporabnikiNaKanalu = io.sockets.clients(kanal);
  var trenutniUporabniki = '';
  for (var i in uporabnikiNaKanalu) {
    var uporabnikSocketId = uporabnikiNaKanalu[i].id;
      if (i > 0) {
         trenutniUporabniki += ', ';
      }
       trenutniUporabniki += vzdevkiGledeNaSocket[uporabnikSocketId];
    }
  
  return trenutniUporabniki;
}

function obdelajZahtevoZaSprememboVzdevka(socket, vzdevkiGledeNaSocket, uporabljeniVzdevki) {
  socket.on('vzdevekSpremembaZahteva', function(vzdevek) {
    if (vzdevek.indexOf('Gost') == 0) {
      socket.emit('vzdevekSpremembaOdgovor', {
        uspesno: false,
        sporocilo: 'Vzdevki se ne morejo začeti z "Gost".'
      });
    } else {
      if (uporabljeniVzdevki.indexOf(vzdevek) == -1) {
        var prejsnjiVzdevek = vzdevkiGledeNaSocket[socket.id];
        var prejsnjiVzdevekIndeks = uporabljeniVzdevki.indexOf(prejsnjiVzdevek);
        uporabljeniVzdevki.push(vzdevek);
        vzdevkiGledeNaSocket[socket.id] = vzdevek;
        delete uporabljeniVzdevki[prejsnjiVzdevekIndeks];
        socket.emit('vzdevekSpremembaOdgovor', {
          uspesno: true,
          vzdevek: vzdevek
        });
        socket.broadcast.to(trenutniKanal[socket.id]).emit('sporocilo', {
          besedilo: prejsnjiVzdevek + ' se je preimenoval v ' + vzdevek + '.'
        });
      } else {
        socket.emit('vzdevekSpremembaOdgovor', {
          uspesno: false,
          sporocilo: 'Vzdevek je že v uporabi.'
        });
      }
    }
  });
}

function obdelajPosredovanjeSporocila(socket) {
  socket.on('sporocilo', function (sporocilo) {
    socket.broadcast.to(sporocilo.kanal).emit('sporocilo', {
      besedilo: vzdevkiGledeNaSocket[socket.id] + ': ' + sporocilo.besedilo
    });
  });
}

// funkcija za obdelovanje zasebnih sporocil
function obdelajPosredovanjeZasebnegaSporocila(socket, uporabljeniVzdevki, vzdevkiGledeNaSocket) {
  socket.on('zasebnoSporocilo', function(sporocilo) {
    var posiljateljZasebnegaSporocila = vzdevkiGledeNaSocket[socket.id];
    var prejemnikZasebnegaSporocila = sporocilo.prejemnik;
    var besediloZasebnegaSporocila = sporocilo.besedilo;
    var posiljateljSocket = socket.id;
    var prejemnikSocket;
    
    // pridobi prejemnikov socked
    for(var index in vzdevkiGledeNaSocket) {
      if(vzdevkiGledeNaSocket[index] == prejemnikZasebnegaSporocila) {
        prejemnikSocket = index;
      }
    }
  
    // ce so pogoji izpolnjeni, poslje sporocilo
    if(uporabljeniVzdevki.indexOf(prejemnikZasebnegaSporocila) != -1 && posiljateljSocket != prejemnikSocket) {
      io.sockets.socket(prejemnikSocket).emit('sporocilo', {
        besedilo: posiljateljZasebnegaSporocila + " (zasebno): " + besediloZasebnegaSporocila
      });
      socket.emit('sporocilo', {
        besedilo: "(Zasebno za " + prejemnikZasebnegaSporocila  + "): " + besediloZasebnegaSporocila
      });
    }
    else {
      socket.emit('sporocilo', {
        besedilo: "Sporočila \"" + besediloZasebnegaSporocila  + "\" uporabniku z vzdevkom " + prejemnikZasebnegaSporocila + " ni bilo mogoče posredovati."
      });
    }
  });
}

function obdelajPridruzitevKanalu(socket) {
  var trenutniKanalName;
  var geslo;
  var trenutniKanalID = trenutniKanal[socket.id];
  
  // preurejena funkcija za pridruzitev prosto dostopnih kanalov
  socket.on('pridruzitevZahteva', function(kanal) {
    trenutniKanalName = kanal.novKanal;
     var trenutniUporabniki = io.sockets.clients(trenutniKanalName);
    
    if(trenutniUporabniki.length >= 1 && seznamGesel[trenutniKanalName] !== "" && seznamGesel[trenutniKanalName] !== undefined) {
      socket.emit('sporocilo', {
        besedilo: 'Pridružitev v kanal ' + trenutniKanalName + ' ni bilo uspešno, ker je geslo napačno!'
      });
    }
    else {
      socket.leave(trenutniKanalID);
      pridruzitevKanalu(socket, trenutniKanalName);
      seznamGesel[trenutniKanalName] = "";
    }
  });
  
  // funkcija za pridruzitev kanalom z gesli
  socket.on('pridruzitevGesloZahteva', function(kanal) {
    trenutniKanalName = kanal.novKanal;
    geslo = kanal.geslo;
    var trenutniUporabniki = io.sockets.clients(kanal.novKanal);
    // geslo je pravilno, pridruzi se kanalu
    if(seznamGesel[trenutniKanalName] == geslo){
      socket.leave(trenutniKanalID);
      pridruzitevKanalu(socket, trenutniKanalName);
    }
    // na tem kanalu ni nobenega uporabnika, keiraj kanal in zapisi geslo
    else  if(trenutniUporabniki.length <= 0){
      socket.leave(trenutniKanalID);
      seznamGesel[trenutniKanalName] = geslo;
      pridruzitevKanalu(socket, trenutniKanalName);
    }
    // vlovimo exception za prosto dostopni kanal
    else  if(seznamGesel[trenutniKanalName] === ""){
      socket.emit('sporocilo', {
        besedilo: 'Izbrani kanal ' + trenutniKanalName + ' je prosto dostopen in ne zahteva prijave z geslom, zato se prijavite z uporabo /pridruzitev ' + trenutniKanalName + ' ali zahtevajte kreiranje kanala z drugim imenom.'
      });
    }
    // vneseno geslo je napacno
    else{
      socket.emit('sporocilo', {
        besedilo: 'Pridružitev v kanal ' + trenutniKanalName + ' ni bilo uspešno, ker je geslo napačno!'
      });
    }
  });
}

function obdelajOdjavoUporabnika(socket) {
  socket.on('odjava', function() {
    var vzdevekIndeks = uporabljeniVzdevki.indexOf(vzdevkiGledeNaSocket[socket.id]);
    delete uporabljeniVzdevki[vzdevekIndeks];
    delete vzdevkiGledeNaSocket[socket.id];
  });
}